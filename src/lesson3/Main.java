package lesson3;

public class Main {
    public static void main(String[] args) {
        /*
        Завдання 1
Створити клас користувача соц мережі User. Задати поля даному класу: id, email та password. Придумати як можна
задати поведінку об’єктам даного класу, наприклад створити поле Boolean isAuthenticated та метод, який буде задавати
значення цій змінній при перевірці полів email та password – true або false. Також можна придумати певні дані, доступ
до яких буде надаватися згідно того, чи аутентифікований юзер.
*/
        User user1 = new User("1@mail.ua", "123");
        User user2 = new User("2@mail.ua", "321");
        User user3 = new User("3@mail.ua", "312");
        user1.helloUser(user1.authorization("asd", "123"));
        user1.helloUser(user1.authorization("1@mail.ua", "1213"));
        user1.helloUser(user1.authorization("1@mail.ua", "123"));
        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user3);
        user1.changeUsername(user1.authorization("asd", "123"), "cc");
        System.out.println(user1);
        user1.changeUsername(user1.authorization("1@mail.ua", "123"), "cc");
        System.out.println(user1);




        /*
Завдання 2
Завантажити файли програми CityBuilder, проаналізувати її, знайти елементи, про які йшла мова в лекції.
Запустити програму в середовищі розробки і взаємодіяти х програмою через вбудовану консоль IDE згідно інструкції.
В консолі операційної системи може працювати некоректно через неналаштовані шрифти. Подивитися результат роботи
програми. Програма призначена для побудови плану міста. Всі елементі розміщені в ряд, але є команда для переводу
рядку на новий. Команди: - h – house, ставить новий будинок - s – snowman, ставить сніговика - f – fountain, ставить
фонтан - g – gas station, ставить заправку - rv – vertical road, ставить квадрат дороги вертикально - rh –
horizontal road, ставить квадрат дороги горизонтально - nl – new line, переводить каретку на новий рядок - save –
зберігає результат, закриває програму та виводить результат в браузер

private final String fountain = "\u26F2"
*/


        /*

Завдання 3
Завдання підвищеної складності. В попередній програмі створіть усього лише один метод, щоб видаляти введений елемент,
якщо він був доданий помилково. Виконання не обов’язкове, швидше на усвідомлення роботи програми.

                    case "d":
                    city[--counter] = "";
                    utils.build(city);
                    break;

*/


        /*
Завдання 4
Створити клас Robot. Задати йому стан з декількох полів, наприклад: рівень заряду, активний, сон, вимкнений…
Створити декілька методів для визначення поведінки об’єкту. Протестувати власну розробку.

         */

        Robot robot1 = new Robot((byte) 66);
        System.out.println(robot1.getChargeLevel());
        robot1.stateofRobot(Robot.StateOfRobot.ACTIVE);
        robot1.stateofRobot(Robot.StateOfRobot.STANDBY);
        robot1.stateofRobot(Robot.StateOfRobot.OFF);
        robot1.setChargeLevel((byte)100);
        System.out.println(robot1.getChargeLevel());
    }
}

class User {
    private final int id;
    private static int counter;
    private String email;
    private String password;
    boolean isAuthenticated;

    public User(String email, String password) {
        this.id = counter++;
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean authorization(String email, String password) {
        this.isAuthenticated = this.email.equals(email) && this.password.equals(password);
                return this.isAuthenticated;
    }

    public void helloUser(boolean isAuthenticated) {
        if (isAuthenticated) {
            System.out.println("Hello User");
        } else {
            System.out.println("Wrong Username or password");
        }
    }

    public void changeUsername(boolean isAuthenticated, String email) {
        if (isAuthenticated) {
            this.email = email;
        } else {
            System.out.println("you don`t have rights");
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", isAuthenticated=" + isAuthenticated +
                ", we have " + counter + " users" +
                '}';
    }
}

class Robot {
    private byte chargeLevel;

    public enum StateOfRobot {ACTIVE, STANDBY, OFF};


    public Robot(byte chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    public byte getChargeLevel() {
        return chargeLevel;
    }

    public void setChargeLevel(byte chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    public void stateofRobot(StateOfRobot state) {
        switch (state) {
            case OFF:
                System.out.println("Turned OFF");
                break;
            case ACTIVE:
                System.out.println("Let`s play");
                break;
            case STANDBY:
                System.out.println("I`ll wait for you");
                break;
        }
    }
}
