package lesson4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        /*
        Завдання 1
Створити програму, що буде деякою подібністю журналу обліку студентів. Вам необхідно створити клас Student, у якого
 будуть поля firstName, secondName, faculty. Створити клас Main, в якому створити метод main для запуску програми.
 В даному методі Вам необхідно створити масив типу Student[] з початковим розміром 10 та створити 8 об’єктів
 студентів з різними іменами, та факультет навчання. Достатньо буде пари факультетів, нехай половина студентів
 вчиться на одному, а інша – на іншому. Додати всіх студентів до масиву. Використовуючи цикл та умовні конструкції
 вивести на екран (в консоль) імена, прізвища студентів лише з одного факультету.
*/
        Student[] studentGroup = new Student[10];

        studentGroup[0] = new Student("A1", "B1", "Java");
        studentGroup[1] = new Student("A2", "B2", "Java");
        studentGroup[2] = new Student("A3", "B3", "Java");
        studentGroup[3] = new Student("A4", "B4", "C");
        studentGroup[4] = new Student("A5", "B5", "C");
        studentGroup[5] = new Student("A6", "B6", "C");
        studentGroup[6] = new Student("A7", "B7", "C");
        studentGroup[7] = new Student("A8", "B8", "Java");


        for (Student iteam : studentGroup) {
            if (iteam != null && iteam.getFaculty().equals("Java")) {
                System.out.println(iteam);
            }
        }

        /*
Завдання 2
Взяти створений Вами додаток у першому завданні та створити в ньому об’єкт Catalog. В цей об’єкт винести логіку по
зберіганню та виведенню на екран студентів по факультетам. Метод виведення на екран має приймати один аргумент з
назвою факультету.
*/

        System.out.println("**********");
        Catalog catalog = new Catalog();
        catalog.printer((catalog.spreadStudentsOnFaculty(studentGroup, "Java")));
        System.out.println("**********");
        catalog.printer((catalog.spreadStudentsOnFaculty(studentGroup, "C")));

        /*
        Не зміг вирішити завдання 2 за вашими вимогами. не розумію, звідки метод принт має отримати масив студентів, якщо
        буде друкувати лише за факультетом. Або потрібно в каталозі створити різні масиви студентів за групами, тоді будемо
        множити кількість масивів та тримати їх у пам'яті. Щось не зрозумів. Якщо можна, покажіть відповідь.
         */



    }
}

        /*
Завдання 3
Виправити стилізацію коду:
public class user { - class має бути з великої літери
    private long user_id;
    private String Username; - username or userName
    private String PassWord; - passWord
    private String FirstName; - firstName
    private String Last_Name; - last_Name
 }
         */

class User {
    private long user_id;
    private String username;
    private String passWord;
    private String firstName;
    private String last_Name;
}

class Student {
    private final String firstName;
    private final String secondName;
    private String faculty;

    public Student(String firstName, String secondName, String faculty) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", faculty='" + faculty + '\'' +
                '}';
    }
}

class Catalog {
    private final int sizeOfGroup = 10;
    private Student[] groupOfStudents = new Student[sizeOfGroup];

    public void setGroupOfStudents(Student[] groupOfStudents) {
        this.groupOfStudents = groupOfStudents;
    }

    public Student[] spreadStudentsOnFaculty(Student[] listOfStudents, String faculty) {
        int counter = 0;
        for (int i = 0; i < listOfStudents.length; i++) {
            if (listOfStudents[i] != null && listOfStudents[i].getFaculty().equalsIgnoreCase(faculty)) {
                groupOfStudents[counter] = listOfStudents[i];
                counter++;
            }
        }
        return groupOfStudents;
    }

    public void printer(Student[] groupOfStudents) {
        for (Student item : groupOfStudents) {
            if (item == null) {
                continue;
            }
            System.out.println(item);
        }
    }

}